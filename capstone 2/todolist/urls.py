from django.urls import path
from . import views


app_name = 'todolist'

urlpatterns = [
    path('', views.index, name='index'),
    path('<int:todoitem_id>/', views.todoitem, name='todoitem'),
    path('eventitem/<int:eventitem_id>/', views.eventitem, name='eventitem'),
    path('update_profile/<int:user_id>', views.update_profile, name='update_profile'),

    
    path('login', views.login_view, name='login'),
    path('logout', views.logout_view, name='logout'),
    path('register', views.register, name='register'),
    path('change_password', views.change_password, name='change_password'),

    
    path('create_task', views.create_task, name="create_task"),
    path('<int:todoitem_id>/edit', views.update_task, name='update_task'),
    path('<int:todoitem_id>/delete', views.delete_task, name='delete_task'),
    
    
    path('create_event', views.create_event, name="create_event"),
]
